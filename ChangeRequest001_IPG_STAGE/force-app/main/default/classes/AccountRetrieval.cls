global with sharing class AccountRetrieval {
    webService static String getAccounts(String cli, String accQuery, String contQuery) {
       String whereac = accQuery.length() > 3 ? (' WHERE ' + accQuery) : '';
       String wherect = contQuery.length() > 3 ? (' WHERE ' + contQuery) : '';

       String sQuery = 'FIND \'' + cli + '\' IN PHONE FIELDS RETURNING Account(id, name' 
           + whereac + '), Contact(id, name' + wherect + ')';
       List<List<SObject>> searchList = search.query(sQuery);
       String JSONString = JSON.serialize(searchList);
       return JSONString;
       //Adding space to check git functionality
       //ADDING coDE TO GIT FROM vscode TO git
    }
}